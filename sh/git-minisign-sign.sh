#!/bin/sh

help() {
    echo "Usage: git-minisign-sign [OPTIONS]"
    echo "Sign a Git commit with Minisign"
    echo
    echo "            -h            show help"
    echo "            -v            output version"
    echo "            -S [SECKEY]   Secret key file (default: ~/.minisign/minisign.key)"
    echo "            -H [HASH]     The hash of commit to verify. If not set, will ask interactively"
    echo "                          Defaults to HEAD. Requires a full hash"
    echo "            -c [COMMENT]  Add a one-line untrusted comment"
    echo "            -t [COMMENT]  Add a one-line trusted comment"
}

version() {
    echo "git-minisign-sign v0.1 by SkyFox42, licensed under MIT"
}

SECKEYFILE="$HOME/.minisign/minisign.key"
TRUSTEDCOMMENT=""
UNTRUSTEDCOMMENT=""
HASH=""

if [ ! -x "$(command -v minisign)" ]; then
    echo "Could not find Minisign. Aborting"
    exit
fi

if [ "$(git rev-parse --is-inside-work-tree 2>/dev/null)" != "true" ]; then
    echo "Current folder is not a Git repo. Aborting"
    exit
fi

while getopts ":S:H:c:t:hv" OPTION 2> /dev/null
do
    case $OPTION in
        h) help
            exit;;
        v) version
            exit;;
        S) SECKEYFILE=$OPTARG;;
        H) HASH=$OPTARG;;
        c) UNTRUSTEDCOMMENT=$OPTARG;;
        t) TRUSTEDCOMMENT=$OPTARG;;
        ?) echo "Option $OPTARG does not exist."
            exit;;
    esac
done
shift  $((OPTIND - 1))

if [[ -z $HASH ]]; then
    echo "Enter the commit hash [HEAD]:"
    read HASH
fi

if [[ -z $HASH ]]; then
    HASH=`git rev-parse HEAD`
    echo "Using HEAD instead (${HASH})"
fi

echo "Checking if the commit is unpacked..."
if [ ! -e ".git/objects/${HASH:0:2}/${HASH:2}" ]; then
    echo "Unpacking the commits..."
    PACKSDIR=$(mktemp -d)
    mv .git/objects/pack/* $PACKSDIR
    git unpack-objects < `echo $PACKSDIR/*.pack`
    rm -rf ${PACKSDIR}
fi

if [ ! -e ".git/objects/${HASH:0:2}/${HASH:2}" ]; then
    echo "The commit does not exist. Aborting."
    exit
fi

echo "Creating the signature..."
if [[ ! -z $TRUSTEDCOMMENT ]] && [[ ! -z $UNTRUSTEDCOMMENT ]]; then
    minisign -Sm ".git/objects/${HASH:0:2}/${HASH:2}" -t "$TRUSTEDCOMMENT" -c "$UNTRUSTEDCOMMENT" -s $SECKEYFILE
elif [[ ! -z $TRUSTEDCOMMENT ]]; then
    minisign -Sm ".git/objects/${HASH:0:2}/${HASH:2}" -t "$TRUSTEDCOMMENT" -s $SECKEYFILE
elif [[ ! -z $UNTRUSTEDCOMMENT ]]; then
    minisign -Sm ".git/objects/${HASH:0:2}/${HASH:2}" -c "$UNTRUSTEDCOMMENT" -s $SECKEYFILE
else
    minisign -Sm ".git/objects/${HASH:0:2}/${HASH:2}" -s $SECKEYFILE
fi

echo "Adding the signature as a note..."
git notes add -F ".git/objects/${HASH:0:2}/${HASH:2}.minisig" $HASH

echo "Removing the signature file..."
rm ".git/objects/${HASH:0:2}/${HASH:2}.minisig"

echo
echo "Done. Don't forget to push the notes:"
echo "git push origin refs/notes/commits"
