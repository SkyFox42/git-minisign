#!/bin/sh

help() {
    echo "Usage: git-minisign-verify [OPTIONS]"
    echo "Verify a Minisign signature of a Git commit"
    echo
    echo "            -h            show help"
    echo "            -v            output version"
    echo "            -P [PUBKEY]   Specify the public key for verification"
    echo "                          If not set, will ask interactively"
    echo "                          If not set via this option or interactively, the program will abort"
    echo "            -H [HASH]     The hash of commit to verify. If not set, will ask interactively"
    echo "                          Defaults to HEAD. Requires a full hash"
}

version() {
    echo "git-minisign-verify v0.1 by SkyFox42, licensed under MIT"
}

PUBKEY=""
HASH=""

if [ ! -x "$(command -v minisign)" ]; then
    echo "Could not find Minisign. Aborting"
    exit
fi

if [ "$(git rev-parse --is-inside-work-tree 2>/dev/null)" != "true" ]; then
    echo "Current folder is not a Git repo. Aborting"
    exit
fi

while getopts ":P:H:hv" OPTION 2> /dev/null
do
    case $OPTION in
        h) help
            exit;;
        v) version
            exit;;
        P) PUBKEY=$OPTARG;;
        H) HASH=$OPTARG;;
        ?) echo "Option $OPTARG does not exist."
            exit;;
    esac
done
shift  $((OPTIND - 1))

if [[ -z $HASH ]]; then
    echo "Enter the commit hash [HEAD]:"
    read HASH
fi

if [[ -z $HASH ]]; then
    HASH=`git rev-parse HEAD`
    echo "Using HEAD instead (${HASH})"
fi

if [[ -z $PUBKEY ]]; then
    echo "Enter the public key:"
    read PUBKEY
fi

if [[ -z $PUBKEY ]]; then
    echo "Public key is required. Aborting"
    exit
fi

echo "Fetching git notes..."
git fetch origin refs/notes/commits:refs/notes/commits

MINISIGFILE=$(mktemp)

echo "Getting the signature for commit ${HASH}..."
git notes show $HASH > $MINISIGFILE

echo "Checking if the commit is unpacked..."
if [ ! -e ".git/objects/${HASH:0:2}/${HASH:2}" ]; then
    echo "Unpacking the commits..."
    PACKSDIR=$(mktemp -d)
    mv .git/objects/pack/* $PACKSDIR
    git unpack-objects < `echo $PACKSDIR/*.pack`
    rm -rf ${PACKSDIR}
fi

if [ ! -e ".git/objects/${HASH:0:2}/${HASH:2}" ]; then
    echo "The commit does not exist. Aborting."
    rm ${MINISIGFILE}
    exit
fi

echo "Verifying the signature..."
echo
minisign -Vm ".git/objects/${HASH:0:2}/${HASH:2}" -x $MINISIGFILE -P $PUBKEY

rm ${MINISIGFILE}
