# git-minisign

Scripts to sign and verify Git commits using Minisign

## How does it work

* `git-minisign-sign.sh`

    Signs the commit object under the given hash and then saves the signature to Git notes. If the commit object with the given hash does not exist, it first tries to unpack `.pack` files in `.git/objects/pack`.

    Once you do `git push`, you also have to do `git push remote_name refs/notes/commits` to push the notes to the repo.

* `git-minisign-verify.sh`

    Fetches commit notes, then extracts the signature from the note of the commit under the given hash, checks if the commit is unpacked, unpacks the commit if needed and then verifies the signature.

## Commit signing

Commits are signed using [Minisign](https://jedisct1.github.io/minisign/) and signatures are stored in git notes.

Pubkey: `RWSxb5gftMn69bFHSOfM7flSRDB/eSjBpz97oXOZD0amGuybqyJ5JYL+`
